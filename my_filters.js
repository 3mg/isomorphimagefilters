var Canvas = require('canvas'),
    Image = Canvas.Image,
    fs = require('fs'),
    Filters = require('./filters.custom.js').Filters,
    //Filters = require('canvasfilters').Filters,
    fabric = require('./fabric.custom.js').fabric;


/*function imgToCanvas(img) {
    var canvas = new Canvas(img.width, img.height);
    var context = canvas.getContext('2d');

    context.drawImage(img, 0, 0, img.width, img.height);
    
    return [canvas, context];
}*/


fabric.Image.filters.Lum = fabric.util.createClass({

    type: 'Lum',

    initialize: function(options) {
        options = options || {};
        this.brightness = options.brightness || 0;
        this.contrast = options.contrast || 1;
    },

    applyTo: function(canvasEl) {
        var context = canvasEl.getContext('2d'),
            imageData = context.getImageData(0, 0, canvasEl.width, canvasEl.height),
            data = imageData.data;

        imageData = Filters.brightnessContrast(imageData, this.brightness, this.contrast);

        context.putImageData(imageData, 0, 0);
    }
});

fabric.Image.filters.Lum.fromObject = function(object) {
    return new fabric.Image.filters.Lum(object);
};


function computeImageViewPort(image) {
    return {
        height: Math.abs(image.getWidth() * (Math.sin(image.getAngle() * Math.PI / 180))) + Math.abs(image.getHeight() * (Math.cos(image.getAngle() * Math.PI / 180))),
        width: Math.abs(image.getHeight() * (Math.sin(image.getAngle() * Math.PI / 180))) + Math.abs(image.getWidth() * (Math.cos(image.getAngle() * Math.PI / 180))),
    }
}

var Crop = {
    applyTransformation: function(canvas, image, next) {
        // Snapshot the image delimited by the crop zone
        var snapshot = new Image();
        snapshot.onload = function() {
            // Validate image
            if (height < 1 || width < 1)
                next(image);

            var imgInstance = new fabric.Image(snapshot, {
                // options to make the image static
                selectable: false,
                evented: false,
                lockMovementX: true,
                lockMovementY: true,
                lockRotation: true,
                lockScalingX: true,
                lockScalingY: true,
                lockUniScaling: true,
                hasControls: false,
                hasBorders: false
            });

            var width = snapshot.width;
            var height = snapshot.height;

            // Update canvas size
            canvas.setWidth(width);
            canvas.setHeight(height);

            // Add image
            image.remove();
            canvas.add(imgInstance);

            next(imgInstance);
        };

        var viewport = computeImageViewPort(image);
        var imageWidth = viewport.width;
        var imageHeight = viewport.height;

        var left = this.options.left * imageWidth;
        var top = this.options.top * imageHeight;
        var width = Math.min(this.options.width * imageWidth, imageWidth - left);
        var height = Math.min(this.options.height * imageHeight, imageHeight - top);

        snapshot.crossOrigin = 'anonymous';
        snapshot.src = canvas.toDataURL({
            left: left,
            top: top,
            width: width,
            height: height,
        });
    }
};

var Rotation = {
    applyTransformation: function(canvas, image, next) {
        var angle = (image.getAngle() + this.options.angle) % 360;
        //console.log(angle);
        image.rotate(angle);

        var width, height;
        height = Math.abs(image.getWidth() * (Math.sin(angle * Math.PI / 180))) + Math.abs(image.getHeight() * (Math.cos(angle * Math.PI / 180)));
        width = Math.abs(image.getHeight() * (Math.sin(angle * Math.PI / 180))) + Math.abs(image.getWidth() * (Math.cos(angle * Math.PI / 180)));

        canvas.setWidth(width);
        canvas.setHeight(height);

        next(image);
    }
};

var Lum = {
    applyTransformation: function(canvas, image, next) {
        var filter;
        for (var i = 0; i < image.filters.length; i++) {
            if (image.filters[i] instanceof fabric.Image.filters.Lum) {
                filter = image.filters[i];
                filter.brightness = this.options.brightness;
                filter.contrast = this.options.contrast;
            }
        }
        if (typeof(filter) === "undefined") {
            filter = new fabric.Image.filters.Lum(this.options);
            image.filters.push(filter);
        }

        var self = this;
        next(image);
    }
};


function bw(sourceCanvas, bwCanvas, threshold) {
    
    bwCanvas.width = sourceCanvas.width;
    bwCanvas.height = sourceCanvas.height;

    var imgData = sourceCanvas.getContext('2d').getImageData(0, 0, sourceCanvas.width, sourceCanvas.height);
    var pixels = imgData.data;

    for (var i = 0, n = pixels.length; i < n; i += 4) {
        var r = pixels[i];
        var g = pixels[i + 1];
        var b = pixels[i + 2];
        var v = (0.2126 * r + 0.7152 * g + 0.0722 * b >= threshold) ? 255 : 0;
        pixels[i] = pixels[i + 1] = pixels[i + 2] = v;

        //pixels[i+3]              is alpha
    }

    bwCanvas.getContext('2d').putImageData(imgData, 0, 0);
}

function gs(sourceCanvas, gsCanvas) {

    gsCanvas.width = sourceCanvas.width;
    gsCanvas.height = sourceCanvas.height;

    var imgData = sourceCanvas.getContext('2d').getImageData(0, 0, sourceCanvas.width, sourceCanvas.height);
    var pixels = imgData.data;

    for (var i = 0, n = pixels.length; i < n; i += 4) {
        var grayscale = pixels[i] * .3 + pixels[i + 1] * .59 + pixels[i + 2] * .11;
        pixels[i] = grayscale; // red
        pixels[i + 1] = grayscale; // green
        pixels[i + 2] = grayscale; // blue
        //pixels[i+3]              is alpha
    }

    gsCanvas.getContext('2d').putImageData(imgData, 0, 0);
}




module.exports = {
    Lum: Lum,
    Rotation: Rotation,
    Crop: Crop,
    
    bw: bw,
    gs: gs
};