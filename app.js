// args: [config.json] [source_image] destination_image [destination_gray_image] [destination_bw_image] [max_w] [max_h]
if (process.argv.length == 2) {
  console.log("args: [config_json_path] [source_image] destination_image [destination_gray_image] [destination_bw_image] [max_w] [max_h]");
  process.exit(1);
}

var Canvas = require('canvas'),
  Image = Canvas.Image,
  fs = require('fs'),
  fabric = require('./fabric.custom.js').fabric,
  myFilters = require("./my_filters.js");
  
var async = require("async");
var _ = require('lodash');

var Lum = myFilters.Lum,
  Crop = myFilters.Crop,
  Rotation = myFilters.Rotation;


process.stdin.resume();
process.stdin.setEncoding('utf8');

var config_json_path, source_image, destination_image, destination_gray_image, destination_bw_image, max_w, max_h;

/*var size = fs.fstatSync(process.stdin.fd).size;
var data = size > 0 ? fs.readSync(process.stdin.fd, size)[0] : '';

if (size == 0) {
  process.stdin.push(null);
}

console.log(size);*/


var data = "";
function checkData(data) {
  if (_.isUndefined(data.transformations) || _.isUndefined(data.threshold)) {
    console.log("config fields 'transformations' and 'threshold' are required");
    process.exit(1);
  }
}


process.stdin.on('data', function(chunk) {
  data += chunk;
});

process.stdin.on('readable', function() {
  if (process.stdin.read() == null) {
    process.stdin.push(null);
  }
});


process.stdin.on('end', function() {


  try {
    var a = process.argv.slice(2);
    data = data != "" ? JSON.parse(data) : null;
  
    if (data == null) {
      if (a.length < 2) {
        console.log("args: [config_json_path] [source_image] destination_image [destination_gray_image] [destination_bw_image] [max_w] [max_h]");
        console.log("both 'config_json_path' and 'destination_image' parameters are required if config not piped!");
        process.exit(1);
      }
  
      config_json_path = a[0];
      data = fs.readFileSync(config_json_path, "utf8");
      data = JSON.parse(data);
  
      a.shift();
    }
    checkData(data);
  
    if (_.isUndefined(data.image)) {
      if (a.length < 2) {
        console.log("args: [config_json_path] [source_image] destination_image [destination_gray_image] [destination_bw_image] [max_w] [max_h]");
        console.log("'source_image' parameter is required if image data not present in config!");
        process.exit(1);
      }
      source_image = a[0];
  
      a.shift();
    }
  
    destination_image = a[0], destination_gray_image = a[1], destination_bw_image = a[2], max_w = a[3], max_h = a[4];
  
    //console.log (a, config_json_path, source_image, destination_image, destination_gray_image, destination_bw_image, max_w, max_h);
    //process.exit(0);
    
    async.waterfall([
      function(asyncCallback) {
  
        if (_.isUndefined(source_image)) {
          asyncCallback(null, data.image);
  
        }
        else {
          fs.readFile(source_image, function(err, squid) {
            if (err) throw err;
            asyncCallback(null, squid);
  
          });
  
        }
  
      },
      function(imageSrc, asyncCallback) {
        var img = new Image;
  
        img.onload = function() {
          var canvas = processImage(img, max_w, max_h);
          save(canvas, destination_image, destination_gray_image, destination_bw_image);
          asyncCallback(null);
        }
  
        img.crossOrigin = 'anonymous';
        img.src = imageSrc;
      }, 
      function() {
        //process.stdout.write("ok");
      }
    ], function(err) {
      throw err;
    });
  
  }
  catch (e) {
    console.log(e.message);
    //console.log(e);
    process.exit(1);
  }

});






function applyTransformation(transformation, canvas, image, next) {

  if (typeof transformation.options.top !== "undefined" &&
    typeof transformation.options.left !== "undefined" &&
    typeof transformation.options.width !== "undefined" &&
    typeof transformation.options.height !== "undefined"
  ) {
    Crop.options = transformation.options;
    Crop.applyTransformation(canvas, image, next);

  }
  else if (typeof transformation.options.angle !== "undefined") {
    Rotation.options = transformation.options;
    Rotation.applyTransformation(canvas, image, next);

  }
  else if ((typeof transformation.options.brightness !== "undefined") && (typeof transformation.options.contrast !== "undefined")) {
    Lum.options = transformation.options;
    Lum.applyTransformation(canvas, image, next);

  }
  else {
    next(image);
  }
}


function save(_canvas, dest, dest_gray, dest_bw) {
  
  var isJpeg = function(path) {
    return (path.indexOf("jpg") == path.length - 3 || path.indexOf("jpeg") - 4);
  }

  var doSave = function (canvas, path) {
    var out = fs.createWriteStream(path);
    var stream = isJpeg(path) ? canvas.createJPEGStream() : canvas.createPNGStream();
  
    stream.on('data', function(chunk) {
      out.write(chunk);
    });
  }
  
  doSave(_canvas, dest);
  
  if (!_.isUndefined(dest_gray)) {
    var grayCanvas = new Canvas();
    myFilters.gs(_canvas, grayCanvas);
    
    doSave(grayCanvas, dest_gray);
  }

  if (!_.isUndefined(dest_bw)) {
    var bwCanvas = new Canvas();
    myFilters.bw(_canvas, bwCanvas, data.threshold);
    
    doSave(bwCanvas, dest_bw);
  }
}

function optimizeSize(image, canvas, maxW, maxH) {
  var w = image.width,
    h = image.height;
  maxW = Math.min(maxW, w), maxH = Math.min(maxH, h);

  var k = h / w;
  var maxWProportional = Math.min(maxW, maxH / k);
  var maxHProportional = Math.min(maxH, maxW * k);

  var newW = w > h ? maxWProportional : maxHProportional / k;
  var newH = h > w ? maxHProportional : maxWProportional * k;

  var scaleX = newW / w,
    scaleY = newH / h;

  if (Math.abs(scaleX - 1) < 0.1 && Math.abs(scaleY - 1) < 0.1) {
    return;
  }

  var filter = new fabric.Image.filters.Resize({
    scaleX: scaleX,
    scaleY: scaleY
  });

  image.filters.push(filter);
}

/**
 * Returns fabric canvas
 */
function processImage(img, maxW, maxH) {
  var canvas = new fabric.createCanvasForNode(img.width, img.height);

  var image = new fabric.Image(img, {
    left: 0,
    top: 0,
    angle: 0,
    opacity: 1
  });

  canvas.setWidth(image.getWidth());
  canvas.setHeight(image.getHeight());

  canvas.add(image);

  var i = 0;
  var f = function(_image) {
    //console.log(i, data.transformations.length);
    if (i < data.transformations.length) {

      if (image != _image) {
        image.remove();

        canvas.centerObject(_image);
        _image.setCoords();
        _image.applyFilters(canvas.renderAll.bind(canvas));

        canvas.add(_image);

        image = _image;
      }

      image.applyFilters(canvas.renderAll.bind(canvas));
      applyTransformation(data.transformations[i++], canvas, _image, f);
    }
  };
  f(image);

  if (!_.isUndefined(maxW) || !_.isUndefined(maxH)) {
    if (_.isUndefined(maxW)) {
      maxW = maxH;
    }
    if (_.isUndefined(maxH)) {
      maxH = maxW;
    }

    optimizeSize(image, canvas, maxW, maxH);
  }

  image.applyFilters(canvas.renderAll.bind(canvas));

  
  /*var width, height, angle = image.angle;
  height = Math.abs(image.getWidth() * (Math.sin(angle * Math.PI / 180))) + Math.abs(image.getHeight() * (Math.cos(angle * Math.PI / 180)));
  width = Math.abs(image.getHeight() * (Math.sin(angle * Math.PI / 180))) + Math.abs(image.getWidth() * (Math.cos(angle * Math.PI / 180)));
  canvas.setWidth(width);
  canvas.setHeight(height);*/

  canvas.centerObject(image);
  image.setCoords();
  
  return canvas;
}