<?php

$config = '
{
  "transformations": [
  {
   "options": {
    "angle": 90
   }
  },
  {
   "options": {
    "angle": 90
   }
  },
  {
   "options": {
    "brightness": "-0.07",
    "contrast": "1.00"
   }
  },
  {
   "options": {
    "brightness": "-0.74",
    "contrast": "1.00"
   }
  },
  {
   "options": {
    "brightness": "-0.37",
    "contrast": "1.00"
   }
  },
  {
   "options": {
    "top": 0.3491855839136938,
    "left": 0.4699812382739212,
    "width": 0.35647279549718575,
    "height": 0.3919587608594247
   }
  },
  {
   "options": {
    "angle": 90
   }
  }
 ],
 "threshold": "44.00"
}
';

$script_args = [
    //"config_json_path" => "",
    "source_image" => __DIR__."/croc.png",
    "destination_image" => __DIR__."/croc_filtered.png",
    "destination_gray_image" => __DIR__."/croc_gray.png",
    "destination_bw_image" => __DIR__."/croc_bw.png",
    //"max_w" => "",
    //"max_h" => "",
];

$spec = array(
    0 => array("pipe", 'r'),  
    1 => array("pipe", 'w'), 
    2 => array("pipe", 'w'), 
);

$proc = proc_open('node app.js '.implode(' ', array_map(function($arg) { return escapeshellarg($arg); }, array_values($script_args))), $spec, $pipes);

fwrite($pipes[0], $config);
fclose($pipes[0]);

$resp = stream_get_contents($pipes[1]);
fclose($pipes[1]);

if (is_resource($proc)) {
    /*do {
        $status = proc_get_status($proc);
        // var_dump($status);
        
        usleep(1);
    } while ($status['running']) ;*/
    
    $status = proc_get_status($proc);
    
    if ($status['exitcode'] != 0) {
        echo stream_get_contents($pipes[2])."\n";
    }
}

proc_close($proc);
echo $resp."\n";